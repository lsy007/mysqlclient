module mysqlclient

go 1.14

require (
	gitee.com/lsy007/mysqlclient v0.0.0-20201102091614-6d7aef86a239
	github.com/golang/protobuf v1.4.3
	github.com/pquerna/ffjson v0.0.0-20190930134022-aa0246cd15f7
	github.com/satori/go.uuid v1.2.0
	github.com/smallnest/rpcx v0.0.0-20200729031544-75f1e2894fdb
	go.uber.org/zap v1.10.0
	gopkg.in/fatih/set.v0 v0.2.1
)
